/*
Created by Jakub Jerzyński
 */

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;

public class TicTacToeGame {

    protected static int[][] board;
    private static int fillSymbol;
    protected static int boardSize;
    private static int numberOfGames;
    private static final String DATE = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
    private static final String PRECISE_DATE = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(new Date());

    private static final String DEF_COLOUR = "\u001B[0m";
    private static final String RED_COLOUR = "\u001B[31m";
    private static final String GREEN_COLOUR = "\u001B[32m";
    private static final String BLUE_COLOUR = "\u001B[34m";
    private static final String YELLOW_COLOUR = "\u001B[33m";

    public static void main (String... args) throws FileNotFoundException, IOException, InterruptedException {

        mainMenuChoice();

    }

    private static void mainMenuText() {
        String menuText;
        String fileName = "resources\\menutext.txt";

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            System.out.println();

            while ((menuText = br.readLine()) != null) {
                System.out.println(menuText);
            }
            br.close();
            
        } catch (IOException exc) {
            errorLog("Błąd wczytywania pliku:\t " + exc.toString());
            System.out.println(RED_COLOUR + "Błąd odczytu pliku menutext.txt:" + DEF_COLOUR + " " + exc);
            System.out.print(YELLOW_COLOUR + "(Menu awaryjne)" + DEF_COLOUR + "1 - Rozpocznij grę, 3 - zakończ program" + "\n");
        }
    }

    private static void mainMenuChoice() throws IOException, InterruptedException {
        int mainMenuChoice;

        logo();
        mainMenuText();
        try {
            System.out.print("Wybierz odpowiednia opcje i nacisnij [Enter]: ");
            mainMenuChoice = new Scanner(System.in).nextInt();
            executeMainMenuChoice(mainMenuChoice);
        } catch (InputMismatchException exc) {
            errorLog("Main menu: \t" + exc.toString());
            executeMainMenuChoice(0);
        }
    }

    private static void executeMainMenuChoice(int mainMenuChoice) throws IOException, InterruptedException {

        switch (mainMenuChoice) {
            case 1:
                logo();
                playGame();
                break;
            case 2:
                howToPlay();
                break;
            case 3:
                System.out.println(RED_COLOUR + "PROGRAM ZAKOŃCZYŁ DZIAŁANIE" + DEF_COLOUR);
                break;
            default:
                mainMenuChoice();
                break;
        }

    }

    private static void playGame() throws IOException, InterruptedException {

        System.out.println("\n\n" + GREEN_COLOUR + "Rozpoczynam nową rozgrywkę" + DEF_COLOUR + "\n");
        setNumberOfGames();
        setBoardSize();

        for (int i = 1; i <= numberOfGames; i++) {

            board = new int[boardSize][boardSize];
            gameRound();
            printBoard(board);

            if (i == numberOfGames) {
                System.out.println("\n\n" + YELLOW_COLOUR + "Rozegrano ustaloną ilość gier!" + DEF_COLOUR);
            } else {
                System.out.println("\n\n" + GREEN_COLOUR + "Rozpoczynam nową rozgrywkę" + DEF_COLOUR + "\n");
            }
        }
        endGameMenuChoice();
    }

    private static int setNumberOfGames() {

        try {
            do {
                System.out.print("Ile gier chcecie zagrać? (min 1): ");
                numberOfGames = new Scanner(System.in).nextInt();
                if (numberOfGames < 1) {
                    System.out.println(RED_COLOUR + "BŁĄD!" + DEF_COLOUR + " Podaj wartość liczbową, minimum 1!");
                }
            } while (numberOfGames < 1);
        } catch (InputMismatchException exc) {
            errorLog("Wyboór ilości gier: \t" + exc.toString());
            System.out.println(RED_COLOUR + "BŁĄD!" + DEF_COLOUR + " Podaj wartość liczbową, minimum 1!");
            setNumberOfGames();
        }
        return numberOfGames;
    }

    private static int setBoardSize() throws IOException {

        try {
            do {
                System.out.print("Jak wielka ma być plansza? (min 3): ");
                boardSize = new Scanner(System.in).nextInt();
                if (boardSize < 3) {
                    System.out.println(RED_COLOUR + "BŁĄD!" + DEF_COLOUR + " Podaj wartość liczbową, minimum 3!");
                }
            } while (boardSize <= 2);
        } catch (InputMismatchException exc) {
            errorLog("Wybór wielkości planszy:\t" + exc.toString());
            System.out.println(RED_COLOUR + "BŁĄD!" + DEF_COLOUR + " Podaj wartość liczbową, minimum 3!");
            setBoardSize();
        }
        gameLog("Wielkość planszy to " + boardSize + "x" + boardSize);
        return boardSize;
    }

    private static void printBoard(int[][] board){

        for(int i = 0; i < board.length * 2 - 1; i++){
            System.out.println();
            
            for(int j = 0; j < board.length * 2 - 1; j++){
                if(i % 2 == 0){
                    if(j % 2 == 0 && board[i/2][j/2] == 0) {
                        System.out.print(" ");
                    } else if (j % 2 == 0 && board[i/2][j/2] == -1) {
                        System.out.print(GREEN_COLOUR + "X" + DEF_COLOUR);
                    } else if (j % 2 == 0 && board[i/2][j/2] == 1) {
                        System.out.print(BLUE_COLOUR + "O" + DEF_COLOUR);
                    }
                    if(j % 2 != 0) System.out.print("|");
                } else {
                    if(j % 2 == 0) System.out.print("-");
                    if(j % 2 != 0) System.out.print("+");   
                }        
            }   
        } System.out.println();    
    }  

    private static int[] setCoordinates(int symbol) {
        int positionInVerticalArray = 0;
        int positionInHorizontalArray = 0;
        boolean coordinatesIsValid = false;
        boolean mismatchSymbol;
        boolean coordinateIsFilled;

        while (!coordinatesIsValid) {
            try {
                mismatchSymbol = false;
                System.out.print("Na jakiej pozycji na osi X chcesz postawić znak: ");
                positionInHorizontalArray = new Scanner(System.in).nextInt() - 1;
                System.out.print("Na jakiej pozycji na osi Y chcesz postawić znak: ");
                positionInVerticalArray = new Scanner(System.in).nextInt() - 1;
            } catch (InputMismatchException exc) {
                errorLog("Podanie koordynat:\t" + exc.toString());
                mismatchSymbol = true;
                positionInVerticalArray = positionInHorizontalArray = -1;
            }

            coordinatesIsValid = positionInHorizontalArray >= 0 && positionInHorizontalArray < boardSize
                    && positionInVerticalArray >= 0 && positionInVerticalArray < boardSize
                    && board[positionInVerticalArray][positionInHorizontalArray] == 0;

            coordinateIsFilled = positionInHorizontalArray >= 0 && positionInHorizontalArray < boardSize
                    && positionInVerticalArray >= 0 && positionInVerticalArray < boardSize
                    && board[positionInVerticalArray][positionInHorizontalArray] != 0;

            if (mismatchSymbol) {
                System.out.println(RED_COLOUR + "BŁĄD!!!" + DEF_COLOUR + " Podałes niedozwolony znak (" + YELLOW_COLOUR + mismatchSymbol + DEF_COLOUR + ")");
            } else if (!coordinatesIsValid && coordinateIsFilled) {
                System.out.println(RED_COLOUR + "BŁĄD!!!" + DEF_COLOUR + " Podałeś koordynaty już zajętego pola (" + YELLOW_COLOUR + coordinateIsFilled + DEF_COLOUR + ")");
            } else if (!coordinatesIsValid && !mismatchSymbol) {
                System.out.println(RED_COLOUR + "BŁĄD!!!" + DEF_COLOUR + " Wyszedłeś poza zakres planszy "
                        + "(" + YELLOW_COLOUR + "x: " + (positionInHorizontalArray + 1) + ", y: " + (positionInVerticalArray + 1) + DEF_COLOUR + ")");
            }
        }
        return new int[]{positionInVerticalArray, positionInHorizontalArray, symbol};
    }

    private static void gameRound() throws IOException, InterruptedException {
        int returnCoordinate[];
        boolean setPlayer;
        char playerSymbol;

        for (int roundCounter = 0; roundCounter < boardSize * boardSize; roundCounter++) {
            setPlayer = roundCounter % 2 == 0;
            if (setPlayer) {
                fillSymbol = -1;
                playerSymbol = 'X';
            } else {
                fillSymbol = 1;
                playerSymbol = 'O';
            }

            printBoard(board);
            System.out.println("\n" + YELLOW_COLOUR + "Runda " + playerSymbol + "" + DEF_COLOUR);
            returnCoordinate = setCoordinates(fillSymbol);
            board[returnCoordinate[0]][returnCoordinate[1]] = fillSymbol;
            gameLog("Runda " + playerSymbol + "\t" + "coord. (x,y): " + (returnCoordinate[0] + 1) + ", " + (returnCoordinate[1]));
            if (chceckWinConditions(fillSymbol, board)) {
                System.out.println("\n" + YELLOW_COLOUR + "Koniec gry. Wygrał: " + playerSymbol + "" + DEF_COLOUR);
                gameLog("Rozgrywkę wygrał " + playerSymbol);
                break;
            }
        }
    }

    private static boolean chceckWinConditions(int fillSymbol, int[][] board) {
        int horizontalWin;
        int verticalWin;
        int upDownDiagonalWin = 0;
        int downUpDiagonalWin = 0;
        boolean win = true;

        for (int i = 0, j = board.length - 1; i < board.length && j >= 0; i++, j--) {
            upDownDiagonalWin += board[i][i];
            downUpDiagonalWin += board[i][j];
        }

        for (int i = 0; i < board.length; i++) {
            horizontalWin = 0;
            verticalWin = 0;

            for (int j = 0; j < board.length; j++) {
                horizontalWin += board[j][i];
                verticalWin += board[i][j];
            }
            boolean someoneWinHorizontal = horizontalWin == board.length * fillSymbol;
            boolean someoneWinVertical = verticalWin == board.length * fillSymbol;
            if (someoneWinHorizontal || someoneWinVertical) {
                return win;
            }
        }

        boolean someoneUpDownDiagonalWin = upDownDiagonalWin == board.length * fillSymbol;
        boolean someonedownUpDiagonalWin = downUpDiagonalWin == board.length * fillSymbol;
        if (someoneUpDownDiagonalWin || someonedownUpDiagonalWin) {
            return win;
        } else {
            return !win;
        }
    }

    private static void endGameMenuText() {
        String endGameText;
        String fileName = "resources\\endgametext.txt";

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            System.out.println();

            while ((endGameText = br.readLine()) != null) {
                System.out.println(endGameText);
            }
            br.close();

        } catch (IOException exc) {
            errorLog("Błąd wczytywania pliku:\t" + exc.toString());
            System.out.println(RED_COLOUR + "Błąd odczytu pliku endgametext.txt:" + DEF_COLOUR + " " + exc);
            System.out.println(YELLOW_COLOUR + "(Menu awaryjne)" + DEF_COLOUR + "1 - Menu główne, 3 - zakończ program");
        }
    }

    private static void endGameMenuChoice() throws IOException, InterruptedException {
        int endGameMenuChoice;

        endGameMenuText();
        try {
            System.out.print("Wybierz opcję i naciśnij [Enter]:");
            endGameMenuChoice = new Scanner(System.in).nextInt();
            executeEndGameMenuChoice(endGameMenuChoice);
        } catch (InputMismatchException exc) {
            errorLog("Wybór w menu końcowym:\t" + exc.toString());
            executeEndGameMenuChoice(0);
        }

    }

    private static void executeEndGameMenuChoice(int endGameMenuChoice) throws IOException, InterruptedException {

        switch (endGameMenuChoice) {
            case 1:
                mainMenuChoice();
                break;
            case 2:
                logo();
                playGame();
                break;
            case 3:
                System.out.println("\n" + RED_COLOUR + "PROGRAM ZAKOŃCZYŁ DZIAŁANIE" + DEF_COLOUR);
                System.exit(0);
                break;
            default:
                screenCleaner();
                endGameMenuChoice();
                break;
        }
    }

    private static void logo() throws IOException, InterruptedException {
        String logo;
        String fileName = "resources\\logo.txt";

        screenCleaner();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            System.out.println();

            while ((logo = br.readLine()) != null) {
                System.out.println(GREEN_COLOUR + logo + DEF_COLOUR);
            }
            br.close();
            
        } catch (IOException exc) {
            errorLog("Błąd wczytywania pliku:\t" + exc.toString());
            System.out.println(RED_COLOUR + "Błąd odczytu pliku logo.txt:" + DEF_COLOUR + " " + exc);
        }
    }

    private static void howToPlay() throws IOException, InterruptedException {
        String rules;
        String fileName = "resources\\howtoplay.txt";
        int returnToMainMenu = 1;

        logo();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            System.out.println();

            while ((rules = br.readLine()) != null) {
                System.out.println(rules);
            }
            br.close();
            
        } catch (IOException exc) {
            errorLog("Błąd wczytywania pliku:\t" + exc.toString());
            System.out.println(RED_COLOUR + "Błąd odczytu pliku howtoplay.txt:" + DEF_COLOUR + " " + exc);
        }

        while (returnToMainMenu != 0) {
            try {
                System.out.print("Wybierz 0 i naciśnij [Enter] by wrócić do menu głównego: ");
                returnToMainMenu = new Scanner(System.in).nextInt();
            } catch (InputMismatchException exc) {
                errorLog("Błąd wyboru opcji w zasadach:\t" + exc.toString());
                returnToMainMenu = 1;
            }
        }
        mainMenuChoice();
    }

    private static void screenCleaner() throws IOException, InterruptedException {
        new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
    }

    private static void errorLog(String excText) {
        String fileName = "resources\\logs\\errorlogs\\" + DATE + " errorLog.txt";

        try(BufferedWriter bw = new BufferedWriter(new FileWriter(fileName, true))){
            
                bw.write("\n\r" + PRECISE_DATE + "  - - -  " + excText + "\r");
                bw.flush();
                
        } catch (IOException exc) {
            System.out.println(RED_COLOUR + "AppError! " + DEF_COLOUR + "Nie mogę wysłać błędu do logów, błąd nie zostanie zapisany");
        }
    }

    private static void gameLog(String moveText) throws IOException {
        String fileName = "resources\\logs\\gamelogs\\" + DATE + " gameLog.txt";

        try(BufferedWriter bw = new BufferedWriter(new FileWriter(fileName, true))){

                bw.write("\n\r" + PRECISE_DATE + "  - - - -  " + moveText + "\r");
                bw.flush();

        } catch (IOException exc) {
            System.out.println(RED_COLOUR + "AppError! " + DEF_COLOUR + "Nie mogę wysłać wprowadzonej opcji do logów! Logi nie zostaną zapisane!");
        }
    }
}
