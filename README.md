Projekt na zaliczenie.

Zadanie:

Stwórz grę w kółko i krzyżyk, używając samych metod. (Bez klas i innych rzeczy związanych z programowaniem obiektowym)

Gra powinna posiadać następujące funkcje:


- Tworzyć plansze o wymiarach podanych przez użytkwnika
- Rozegrać ilość gier podaną przez użytkownika
- Gracz nie może nadpisać znaku drugiego gracza i nie może przy tym tracić tury
- Gra powinna zakończyć się wygraną lub remisem
- Gra powinna sprawdzać czy dany gracz wygrał - pionowe, poziome i skośne warunki zwycięstwa
- Rozgrywka nie powinna trwać dłużej niż ilość dostepnych pół


Rzeczy stworzone dodatkowo (niewymagane):


- stworzyem logo gry w formie ASCII artu
- stworzyłem menu główne gry oraz menu po zakończeniu rozgrywki
- wprowadziłem przechytywanie i obsługę błędów
- wprowadziłem kolorowe znaki O i X, w celu zwiększenia czytelności planszy
- stworzyłem metodę czyszczącą ekran, dla zwiększenia czytelności gry w konsoli
- stworzyłem metodę przesyłającą przechwycone błędy do logów
- stworzyłem metodę przesyłającą przebieg rozgrywki do logów


#### Moje opublikowane projekty
1. [Simple RPG Game](https://gitlab.com/jjerzynski/SimpleRpgGame) - (w produkcji) Okienkowa gra RPG stylizowana na produkcje z lat '80-'90, mój największy projekt;
2. [Weather App](https://gitlab.com/jjerzynski/SimpleWeatherApp) - (projekt zakończony) Konsolowa aplikacja pogodowa oparna na REST;
3. [Weather App 2](https://gitlab.com/jjerzynski/RESTWeatherApp) - (w produkcji) Ulepszona aplikacja pogodowa w oparciu o REST i JSON oraz Swinga;
4. [Mastermind](https://gitlab.com/jjerzynski/MasterMindConsole) - (projekt zakończony) Gra logiczna z lat '80;
5. [TicTacToe](https://gitlab.com/jjerzynski/TicTacToeGame) - (projekt zakończony) Kóło i krzyżyk - projekt na zaliczenie zajęc w Craftin' Code, mój pierwszy projekt;